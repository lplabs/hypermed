module.exports = function (overrides) {
    return function (req, options, next, hypermed) {
        var override = overrides[req.method];
        if (override) {
            req.headers["X-HTTP-Method-Override"] = req.method;
            req.method = override;
        }

        return next();
    };
}