var _ = require('lodash');
var url = require('url');

var BODY_METHODS = {
    "POST": true,
    "PUT": true
};

/**
 * Replaces every :param in the URL path with the value in the parameters, any remaining parameters
 * are added to the URL parameters (or into the request body for POST/PUT unless forceURL is true)
 * @param defaultParams
 * @returns {Function}
 */
module.exports = function ParamsFilter(defaultParams, forceURL) {
    return function(req, options, next, hypermed) {
        if(defaultParams || options.params) {
            var target = req.url;
            target = req.url = typeof target === "string" ? url.parse(req.url, true) : target;
            delete target.search;

            var params = _.extend({}, defaultParams, options.params || {});
            var urlParamsTarget = (!(forceURL || options.ParamsFilter && options.ParamsFilter.forceURL) && BODY_METHODS[req.method]) ? (req.form = req.form || {}) : req.url.query;

            _.forOwn(params, function(val, key) {
                var placeholder = ":"+key;
                if(target.path.indexOf(placeholder) !== -1) {
                    target.path = target.path.replace(new RegExp(placeholder,"g"), String(val));
                }
                else {
                    urlParamsTarget[key] = val;
                }
            });
        }
        return next();
    };
}