var request = require('request');
//request.debug = true;
var Q = require('q');
var _ = require('lodash');
var url = require('url');

var entity = require('./entity');

var qrequest = Q.nfbind(request);

function attachCB(promise, cb) {
    if (cb) {
        promise.then(function (res) {
            cb(null, res);
        }, function (err) {
            cb(err || new Error());
        });
    }
    ;
    return promise;
}
function attachSpreadCB(promise, cb) {
    if (cb) {
        promise.spread(function (res) {
            var args = Array.prototype.slice.call(arguments);
            args.splice(0, 0, null);
            cb.apply(null, args);
        }, function (err) {
            cb(err || new Error());
        });
    }
    ;
    return promise;
}

var Hypermed = function Hypermed(_defaults) {
    var me = this;
    _defaults = this._defaults = _.extend({
        method: "GET",
        followRedirect: true,
        json: true,
        entityKey: false
    }, _defaults);

    var _filters = this._filters = [];

    this.use = function (filter) {
        if (_.isArray(filter)) {
            filter.forEach(function (filter) {
                _filters.push(filter);
            });
        }
        else if (typeof filter === "function") {
            _filters.push(filter);
        }

        return me;
    };


    this._resolveOptions = function (extras) {
        return _.extend({}, _defaults, extras);
    };

    ["send", "extend", "path", "get", "create", "set", "asEntity"].forEach(function bindMethod(method) {
        me[method] = Hypermed.prototype[method].bind(me);
    })

};


/**
 *
 * @param options
 * @param cb
 * @returns {*}
 */
Hypermed.prototype.send = function (options, cb) {
    var me = this;
    options = me._resolveOptions(options);
    var req = {
        method: options.method,
        url: options.url,
        timeout: options.timeout,

        //Auth
        auth: options.auth,

        //Request data:
        headers: options.headers || {},
        formData: options.formData,
        body: options.body,
        json: options.json,

        //Response
        followRedirect: options.followRedirect,
        maxRedirects: options.maxRedirects,
        encoding: options.encoding
    };

    var filterList = me._filters.concat([function (req, options) {
//        console.log("Sending", req)
        return qrequest(req).then(function (values) {
            values.push(options);
            return values;
        });
    }]);

    var i = 0;

    function next(newReq) {
        if (newReq instanceof Error) {
            return Q.reject(newReq);
        }

        var currentFunction = filterList[i];
        ++i;
        req = newReq || req;
        try {
            return currentFunction(req, options, next, me);
        }
        catch (err) {
            return Q.reject(err);
        }
    }


    var promiseResult = next(req);


    return attachSpreadCB(promiseResult, cb);
};

Hypermed.prototype.extend = function (options, filters) {
    return new Hypermed(_.extend({}, this._defaults, options || {})).use(this._filters).use(filters || []);
};

Hypermed.prototype.path = function (path, options) {
    var target = this._defaults.url;
    if (!target) {
        target = path;
    }
    else {
        if (typeof target !== "string") {
            target = url.format(target);
        }
        target = url.resolve(target, path);
    }

    var newOptions = _.extend({}, options || {}, { url: target });
    return this.extend(newOptions);
};

function Resource() {

}


Hypermed.prototype.get = function (params, options) {
    var hypermed = this;

    return this.send(_.extend({method: "GET", params: params || {}}, options || {})).spread(function (response, body, options) {
        if (response.statusCode !== 200) {
            throw new Error("Unexpected status code for get " + response.statusCode + ":\n" + JSON.stringify(body));
        }

        if (options.entityKey) {
            var entityKey = options.entityKey;
            if (typeof options.entityKey !== "string") {
                entityKey = Object.keys(body)[0];
            }
            body = Object(body[entityKey]); //Wrapping primitives to objects so we can add the link property.
        }
        return hypermed.asEntity(body);
    });
};

Hypermed.prototype.asEntity = function (object) {
    var hypermed = this;

    var links = (object.link || []);
    var linkMap = {};
    var linkList = [];

    links.forEach(function (link) {
        var rel = link["rel"] || link["@rel"];
        var href = link["href"] || link["@href"];

        linkList.push({
            rel: rel,
            href: href
        });
        linkMap[rel] = href;
    });

    object.link = function (rel, options) {
        var href = linkMap[rel];
        return href && hypermed.path(href, options);
    };
    object.link.all = linkList;
    object.link.map = linkMap;
    return object;
};

Hypermed.prototype.create = function (body, options) {
    return this.send(_.extend({method: "POST", follow: false, body: body, expectedCode: 201}, options || {})).spread(function (response, body, options) {
        if (response.statusCode !== options.expectedCode) {
            throw new Error("Unexpected status code for create " + response.statusCode + ":\n" + JSON.stringify(body));
        }
        return response.headers.location;
    });
};
Hypermed.prototype.set = function (body, options) {
    return this.send(_.extend({method: "PUT", follow: false, body: body, expectedCode: 200}, options || {})).spread(function (response, body, options) {
        if (response.statusCode !== options.expectedCode) {
            throw new Error("Unexpected status code for set " + response.statusCode + ":\n" + JSON.stringify(body));
        }
        return undefined; //Just to be explicit; resolved with undefined.
    });
};


Hypermed.MethodOverrides = require("./filters/method-overrides.js");
Hypermed.ParamsFilter = require("./filters/params-filter.js");

module.exports = Hypermed;


